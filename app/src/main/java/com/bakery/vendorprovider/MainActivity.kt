package com.bakery.vendorprovider

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bakery.vendorprovider.UiUtils.BottomNavigationViewHelper
import com.bakery.vendorprovider.adapter.HomeViewPagerAdapter
import com.bakery.vendorprovider.fragment.AccountFragment
import com.bakery.vendorprovider.fragment.OrdersFragment
import com.bakery.vendorprovider.fragment.RideFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        init()

    }

    private fun init() {
        BottomNavigationViewHelper.removeShiftMode(bottomNavigation)
        val adapter = HomeViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(RideFragment(), "")
        adapter.addFragment(OrdersFragment(), "")
        adapter.addFragment(AccountFragment(), "")
        viewPager.adapter = adapter

        bottomNavigation.setOnNavigationItemSelectedListener(BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.ride_item -> {
                    viewPager.currentItem = 0
                    return@OnNavigationItemSelectedListener true
                }
                R.id.orders_item -> {
                    viewPager.currentItem = 1
                    return@OnNavigationItemSelectedListener true
                }
                R.id.account_item -> {
                    viewPager.currentItem = 2
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        })

    }
}